USE [master]
GO

CREATE DATABASE [OdbojkaskiTim] 
GO

USE [OdbojkaskiTim] 
GO

CREATE TABLE [dbo].[EvidencijaIgraca](
	[JMBG] [int] NOT NULL PRIMARY KEY AUTOINCREMENT,
	[Ime] [nvarchar](40) NOT NULL,
	[Prezime] [nvarchar](40) NOT NULL,
        [DatumRodjenja] [nvarchar] (40) NOT NULL,
        [BrojDresa] [int] NOT NULL,
        [DrzavnoPoreklo] [nvarchar] (40) NOT NULL,
        [IdTima] [int] NOT NULL
)
GO

CREATE TABLE [dbo].[Tim](
	[IdTima] [int]  NOT NULL PRIMARY KEY,
	[NazivTima] [nvarchar](40) NOT NULL,
        [LigaTima] [nvarchar] (40) NOT NULL,
        [GradTima] [nvarchar] (40) NOT NULL,
        [BrojTrofeja] [int] NOT NULL 
)
GO

CREATE TABLE [dbo].[Korisnik](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Prezime] [nvarchar](40) NOT NULL,
	[Ime] [nvarchar](30) NOT NULL,
	[KorisnickoIme] [nvarchar](20) NOT NULL,
	[Sifra] [nvarchar](30) NOT NULL,
	[Status] [nvarchar](10) NOT NULL
)

GO 

 
ALTER TABLE [dbo].[EvidencijaIgraca] ADD CONSTRAINT
[FK_EvidencijaIgraca_Tim] FOREIGN KEY([IdTima)
REFERENCES [dbo].[Tim] ([IdTima])
ON UPDATE CASCADE
GO