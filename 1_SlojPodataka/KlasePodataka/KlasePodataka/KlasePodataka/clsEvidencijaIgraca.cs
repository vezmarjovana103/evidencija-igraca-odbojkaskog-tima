﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlasePodataka
{
    public class clsEvidencijaIgraca
    {
       
        private int pJMBG;
        private string pIme;
        private string pPrezime;
        private string pDatumRodjenja;
        private int pBrojDresa;
        private string pDrzavnoPoreklo;

        private clsTim objTim;


        public int JMBG
        {
            get { return pJMBG; }
            set { pJMBG = value; }
        }

        public string Ime
        {
            get { return pIme; }
            set { pIme = value; }
        }

        public string Prezime
        {
            get { return pPrezime; }
            set { pPrezime = value; }
        }


        public string DatumRodjenja
        {
            get { return pDatumRodjenja; }
            set { pDatumRodjenja = value; }
        }

        public int BrojDresa
        {
            get { return pBrojDresa; }
            set { pBrojDresa = value; }
        }


        public string DrzavnoPoreklo
        {
            get { return pDrzavnoPoreklo; }
            set { pDrzavnoPoreklo = value; }
        }
        

        public clsTim Tim
        {
            get { return objTim; }
            set { objTim = value; }
        }
    }
}
