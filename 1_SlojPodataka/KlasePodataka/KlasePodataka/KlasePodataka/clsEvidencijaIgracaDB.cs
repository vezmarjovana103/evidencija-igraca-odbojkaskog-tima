﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace KlasePodataka
{
    public class clsEvidencijaIgracaDB
    {
      
        private string pStringKonekcije;

       
        public string StringKonekcije
        {
            get
            {
                return pStringKonekcije;
            }
        }
      
        public clsEvidencijaIgracaDB(string NoviStringKonekcije)
     
        {
            pStringKonekcije = NoviStringKonekcije; 
        }

       
        public DataSet DajSveEvidencijeIgraca()
        {
          
            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajSveEvidencijeIgracaSaJoin", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();
            
            return dsPodaci;
        }

        public DataSet DajEvidencijeIgracaPoPrezimenu(string Prezime)
        {
            
            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajEvidencijuIgracaPoPrezimenu", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@Prezime", SqlDbType.NVarChar).Value = Prezime;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();

            return dsPodaci;
        }


        public DataSet DajEvidencijuIgracaPoJMBG(int JMBG)
        {
            
            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajEvidencijuIgracaPoJMBG", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@JMBG", SqlDbType.Int).Value = JMBG;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();

            return dsPodaci;
        }

        public int DajUkupnoIgracaPoIdTima(int IdTima)
        {
            int ukupnoIgraca = 0;
            DataSet dsPodaci = new DataSet();
            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajUkupnoIgracaPoIdTima", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@IdTima", SqlDbType.Int).Value = IdTima;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();
            ukupnoIgraca = int.Parse(dsPodaci.Tables[0].Rows[0].ItemArray[0].ToString());
            return ukupnoIgraca;
        } 


        private clsEvidencijaIgracaLista DajListuSvihEvidencijaIgraca()
        {
            // PRIPREMA PROMENLJIVIH
            clsEvidencijaIgracaLista objEvidencijaIgracaLista = new clsEvidencijaIgracaLista();
            DataSet dsPodaciEvidencijaIgraca = new DataSet();
            clsEvidencijaIgraca objEvidencijaIgraca;
            clsTim objTim;

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajSveEvidencijeIgracaSaJoinIdTima", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaciEvidencijaIgraca);
            Veza.Close();
            Veza.Dispose();

            // FORMIRANJE OBJEKATA I UBACIVANJE U LISTU
            for (int brojac = 0; brojac < dsPodaciEvidencijaIgraca.Tables[0].Rows.Count; brojac++)
            {
                objTim = new clsTim();
                objTim.IdTima = int.Parse(dsPodaciEvidencijaIgraca.Tables[0].Rows[brojac].ItemArray[4].ToString());
                objTim.NazivTima = dsPodaciEvidencijaIgraca.Tables[0].Rows[brojac].ItemArray[3].ToString();
                objTim.LigaTima = dsPodaciEvidencijaIgraca.Tables[0].Rows[brojac].ItemArray[3].ToString();
                objTim.GradTima = dsPodaciEvidencijaIgraca.Tables[0].Rows[brojac].ItemArray[3].ToString();
                objTim.BrojTrofeja = int.Parse(dsPodaciEvidencijaIgraca.Tables[0].Rows[brojac].ItemArray[3].ToString());

                objEvidencijaIgraca = new clsEvidencijaIgraca();
                objEvidencijaIgraca.JMBG = int.Parse(dsPodaciEvidencijaIgraca.Tables[0].Rows[brojac].ItemArray[0].ToString());
                objEvidencijaIgraca.Ime = dsPodaciEvidencijaIgraca.Tables[0].Rows[brojac].ItemArray[0].ToString();
                objEvidencijaIgraca.Prezime = dsPodaciEvidencijaIgraca.Tables[0].Rows[brojac].ItemArray[0].ToString();
                objEvidencijaIgraca.DatumRodjenja = dsPodaciEvidencijaIgraca.Tables[0].Rows[brojac].ItemArray[0].ToString();
                objEvidencijaIgraca.BrojDresa = int.Parse(dsPodaciEvidencijaIgraca.Tables[0].Rows[brojac].ItemArray[0].ToString());
                objEvidencijaIgraca.DrzavnoPoreklo = dsPodaciEvidencijaIgraca.Tables[0].Rows[brojac].ItemArray[0].ToString();
                
                objEvidencijaIgraca.Tim = objTim;
                objEvidencijaIgracaLista.DodajElementListe(objEvidencijaIgraca);
            }

            return objEvidencijaIgracaLista;
        }


        public bool SnimiNovuEvidencijuIgraca(clsEvidencijaIgraca objNovaEvidencijaIgraca)
        {
            
            int brojSlogova =0;

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();

            SqlCommand Komanda = new SqlCommand("DodajNovuEvidencijuIgraca", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@JMBG", SqlDbType.Int).Value = objNovaEvidencijaIgraca.JMBG;
            Komanda.Parameters.Add("@Prezime", SqlDbType.NVarChar).Value = objNovaEvidencijaIgraca.Prezime;
            Komanda.Parameters.Add("@Ime", SqlDbType.NVarChar).Value = objNovaEvidencijaIgraca.Ime;
            Komanda.Parameters.Add("@DatumRodjenja ", SqlDbType.NVarChar).Value = objNovaEvidencijaIgraca.DatumRodjenja;
            Komanda.Parameters.Add("@BrojDresa ", SqlDbType.Int).Value = objNovaEvidencijaIgraca.BrojDresa;
            Komanda.Parameters.Add("@DrzavnoPoreklo ", SqlDbType.NVarChar).Value = objNovaEvidencijaIgraca.DrzavnoPoreklo;
           
            brojSlogova = Komanda.ExecuteNonQuery();
            Veza.Close();
            Veza.Dispose();
     
           
            return (brojSlogova > 0);

        }

        public bool ObrisiEvidencijuIgraca(int JMBGZaBrisanje)
        {
           
            int brojSlogova = 0;
           

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();

            SqlCommand Komanda = new SqlCommand("ObrisiEvidencijuIgraca", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@JMBG", SqlDbType.Int).Value = JMBGZaBrisanje;
            brojSlogova = Komanda.ExecuteNonQuery();
            Veza.Close();
            Veza.Dispose();

            return (brojSlogova > 0);

        }

        public bool IzmeniEvidencijuIgraca(clsEvidencijaIgraca objStaraEvidencijaIgraca, clsEvidencijaIgraca objNovaEvidencijaIgraca)
        {
           
            int brojSlogova = 0;
           

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();

            SqlCommand Komanda = new SqlCommand("IzmeniEvidencijuIgraca", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@StariJMBG", SqlDbType.Int).Value = objStaraEvidencijaIgraca.JMBG;
            Komanda.Parameters.Add("@JMBG", SqlDbType.Int).Value = objNovaEvidencijaIgraca.JMBG;
            Komanda.Parameters.Add("@Prezime", SqlDbType.NVarChar).Value = objStaraEvidencijaIgraca.Prezime;
            Komanda.Parameters.Add("@Ime", SqlDbType.NVarChar).Value = objStaraEvidencijaIgraca.Ime;
            Komanda.Parameters.Add("@DatumRodjenja", SqlDbType.Int).Value = objStaraEvidencijaIgraca.DatumRodjenja;
            Komanda.Parameters.Add("@DrzavnoPoreklo", SqlDbType.NVarChar).Value = objStaraEvidencijaIgraca.DrzavnoPoreklo;

            brojSlogova = Komanda.ExecuteNonQuery();
            Veza.Close();
            Veza.Dispose();

            return (brojSlogova > 0);

        }

        


    }
}
