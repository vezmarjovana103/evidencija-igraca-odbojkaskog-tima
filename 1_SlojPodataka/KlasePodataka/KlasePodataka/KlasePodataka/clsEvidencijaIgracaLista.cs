﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlasePodataka
{
    public class clsEvidencijaIgracaLista
    {

        
        private List<clsEvidencijaIgraca> pListaEvidencijeIgraca;


        public List<clsEvidencijaIgraca> ListaEvidencijeIgraca
        {
            get
            {
                return pListaEvidencijeIgraca;
            }
            set
            {
                if (this.pListaEvidencijeIgraca != value)
                    this.pListaEvidencijeIgraca = value;
            }
        }

      
        public clsEvidencijaIgracaLista()
        {
            pListaEvidencijeIgraca = new List<clsEvidencijaIgraca>(); 

        }

       
        public void DodajElementListe(clsEvidencijaIgraca objNovaEvidencijaIgraca)
        {
            pListaEvidencijeIgraca.Add(objNovaEvidencijaIgraca);
        }

        public void ObrisiElementListe(clsEvidencijaIgraca objEvidencijaIgracaZaBrisanje)
        {
            pListaEvidencijeIgraca.Remove(objEvidencijaIgracaZaBrisanje);  
        }

        public void ObrisiElementNaPoziciji(int pozicija)
        {
            pListaEvidencijeIgraca.RemoveAt(pozicija);
        }

        public void IzmeniElementListe(clsEvidencijaIgraca objStaraEvidencijaIgraca, clsEvidencijaIgraca objNovaEvidencijaIgraca)
        {
            int indexStareEvidencijeIgraca = 0;
            indexStareEvidencijeIgraca = pListaEvidencijeIgraca.IndexOf(objStaraEvidencijaIgraca);
            pListaEvidencijeIgraca.RemoveAt(indexStareEvidencijeIgraca);
            pListaEvidencijeIgraca.Insert(indexStareEvidencijeIgraca, objNovaEvidencijaIgraca);   
        }

           
    }
}
