﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlasePodataka
{
    public class clsTim
    {
      
        private int pIdTima;
        private string pNazivTima;
        private string pLigaTima;
        private string pGradTima;
        private int pBrojTrofeja;


        public int IdTima
        {
            get
            {
                return pIdTima;
            }
            set
            {
                if (this.pIdTima != value)
                    this.pIdTima = value;
            }
        }

        public string NazivTima
        {
            get
            {
                return pNazivTima;
            }
            set
            {
                if (this.pNazivTima != value)
                    this.pNazivTima = value;
            }
        }

        public string LigaTima
        {
            get
            {
                return pLigaTima;
            }
            set
            {
                if (this.pLigaTima != value)
                    this.pLigaTima = value;
            }
        }

        public string GradTima
        {
            get
            {
                return pGradTima;
            }
            set
            {
                if (this.pGradTima != value)
                    this.pGradTima = value;
            }
        }


        public int BrojTrofeja
        {
            get
            {
                return pBrojTrofeja;
            }
            set
            {
                if (this.pBrojTrofeja != value)
                    this.pBrojTrofeja = value;
            }
        }

        public clsTim()
        {
          pIdTima=0;
          pNazivTima="";
          pLigaTima="";
          pGradTima="";
          pBrojTrofeja = 0;
        }
    }
}
