﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace KlasePodataka
{
    public class clsTimDB
    {
       
        private string pStringKonekcije;

        
        public string StringKonekcije
        {
            get
            {
                return pStringKonekcije;
            }
            set 
            {
                if (this.pStringKonekcije != value)
                    this.pStringKonekcije = value;
            }
        }

        public clsTimDB(string NoviStringKonekcije)
      
        {
            pStringKonekcije = NoviStringKonekcije; 
        }


        public DataSet DajSveTimove()
        {

            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajSveTimove", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();
            
            return dsPodaci;
        }

        /*public DataSet DajTimPoNazivu(string NazivTima)
        {
          
            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajTimPoNazivu", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@NazivTima", SqlDbType.NVarChar).Value = NazivTima;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();

            //Prezime = dsPodaci.Tables[0].Rows[0].ItemArray[1].ToString();

            return dsPodaci;
        }*/

        public string DajNazivTimaPremaIdTima(int IdTima)
        {
           string NazivTima = "";
            DataSet dsPodaci = new DataSet();

           SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajNazivTimaPremaIdTima", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@IdTima", SqlDbType.Int).Value = IdTima;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();

            NazivTima = dsPodaci.Tables[0].Rows[0].ItemArray[1].ToString();

            return NazivTima;
        }

        public string DajIdTimaPoNazivu(string NazivTima)
        {

            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajTimPoNazivu", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@NazivTima", SqlDbType.NVarChar).Value = NazivTima;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();

            return dsPodaci.Tables[0].Rows[0].ItemArray[0].ToString();
        }



        public DataSet DajTimPoNazivu(string NazivTima)
        {

            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajTimPoNazivu", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@NazivTima", SqlDbType.NVarChar).Value = NazivTima;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();

            return dsPodaci;
        }

        public DataSet DajTimPoNazivu(clsTim objTimZaFilter)
        {

            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajTimPoNazivu", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@NazivTima", SqlDbType.NVarChar).Value = objTimZaFilter.NazivTima;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();

            return dsPodaci;
        }




       /* public string DajIdTimaPoNazivu(string NazivTima)
        {
        
            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajTimPoNazivu", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@NazivTima", SqlDbType.NVarChar).Value = NazivTima;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();

            return dsPodaci.Tables[0].Rows[0].ItemArray[0].ToString () ;
        }*/

/*
      /*  public DataSet DajIdTimaPoNazivu(string Naziv)
        {
            // MOGU biti jos neke procedure, mogu SE VRATITI VREDNOSTI I U LISTU, DATA TABLE...
            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajTimPoNazivu", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@NazivTima", SqlDbType.NVarChar).Value = Naziv;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();

            return dsPodaci;
        }*/

        /*
        // overloading metoda - isto se zove, ima drugaciji parametar
        public DataSet DajTimPoNazivu(clsTim objTimZaFilter)
        {
            // MOGU biti jos neke procedure, mogu SE VRATITI VREDNOSTI I U LISTU, DATA TABLE...
            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajZvanjePoNazivu", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@Naziv", SqlDbType.NVarChar).Value = objTimZaFilter.NazivTima;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();

            return dsPodaci;
        }
        */
    
        


        /*
        public DataSet DajGledaocaPoPrezimenu(string Prezime)
        {
      
            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajGledaocaPoPrezimenu", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@Prezime", SqlDbType.NVarChar).Value = Prezime;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();

            return dsPodaci;
        }
        

        public DataSet DajTimPoPrezimenu(clsTim objTimZaFilter)
        {
            
            DataSet dsPodaci = new DataSet();

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();
            SqlCommand Komanda = new SqlCommand("DajGledaocaPoPrezimenu", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@Prezime", SqlDbType.NVarChar).Value = objTimZaFilter.Prezime;
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = Komanda;
            da.Fill(dsPodaci);
            Veza.Close();
            Veza.Dispose();

            return dsPodaci;
        }*/

        public bool SnimiNoviTim(clsTim objNovTim)
        {
            
            int brojSlogova =0;
           

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();

            SqlCommand Komanda = new SqlCommand("DodajNoviTim", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@IdTima", SqlDbType.Int).Value = objNovTim.IdTima;
            Komanda.Parameters.Add("@NazivTima", SqlDbType.NVarChar).Value = objNovTim.NazivTima;
            Komanda.Parameters.Add("@LigaTima ", SqlDbType.NVarChar).Value = objNovTim.LigaTima;
            Komanda.Parameters.Add("@GradTima", SqlDbType.NVarChar).Value = objNovTim.GradTima;
            Komanda.Parameters.Add("@BrojTrofeja", SqlDbType.Int).Value = objNovTim.BrojTrofeja;


            brojSlogova = Komanda.ExecuteNonQuery();
            Veza.Close();
            Veza.Dispose();

           
            return (brojSlogova > 0);

          
            
        }

        public bool ObrisiTim(clsTim objTimZaBrisanje)
        {
           
            int brojSlogova = 0;
            

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();

            SqlCommand Komanda = new SqlCommand("ObrisiTim", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@IdTima", SqlDbType.Int).Value = objTimZaBrisanje.IdTima;
            brojSlogova = Komanda.ExecuteNonQuery();
            Veza.Close();
            Veza.Dispose();

            return (brojSlogova > 0);

         }


        public bool ObrisiTim(int IdTimZaBrisanje)
        {
        
            int brojSlogova = 0;
           
            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();

            SqlCommand Komanda = new SqlCommand("ObrisiTim", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@IdTima", SqlDbType.Int).Value = IdTimZaBrisanje;
            brojSlogova = Komanda.ExecuteNonQuery();
            Veza.Close();
            Veza.Dispose();

            return (brojSlogova > 0);

        }

        public bool IzmeniTim(clsTim objStariTim, clsTim objNoviTim)
        {
           
            int brojSlogova = 0;
            

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();

            SqlCommand Komanda = new SqlCommand("IzmeniTim", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@StariIdTima", SqlDbType.Int).Value = objStariTim.IdTima;
            Komanda.Parameters.Add("@IdTima", SqlDbType.Int).Value = objNoviTim.IdTima;
            Komanda.Parameters.Add("@NazivTima", SqlDbType.NVarChar).Value = objNoviTim.NazivTima;
            Komanda.Parameters.Add("@LigaTima", SqlDbType.NVarChar).Value = objNoviTim.LigaTima;
            Komanda.Parameters.Add("@GradTima ", SqlDbType.NVarChar).Value = objNoviTim.GradTima;
            Komanda.Parameters.Add("@BrojTrofeja", SqlDbType.Int).Value = objNoviTim.BrojTrofeja;
            brojSlogova = Komanda.ExecuteNonQuery();
            Veza.Close();
            Veza.Dispose();

            return (brojSlogova > 0);

        }


        public bool IzmeniTim(int IdStarogTima, clsTim objNoviTim)
        {
         
            int brojSlogova = 0;
           

            SqlConnection Veza = new SqlConnection(pStringKonekcije);
            Veza.Open();

            SqlCommand Komanda = new SqlCommand("IzmeniTim", Veza);
            Komanda.CommandType = CommandType.StoredProcedure;
            Komanda.Parameters.Add("@StariIdTima", SqlDbType.Int).Value = IdStarogTima;
            Komanda.Parameters.Add("@IdTima", SqlDbType.Int).Value = objNoviTim.IdTima;
            Komanda.Parameters.Add("@NazivTima", SqlDbType.NVarChar).Value = objNoviTim.NazivTima;
            Komanda.Parameters.Add("@LigaTima", SqlDbType.NVarChar).Value = objNoviTim.LigaTima;
            Komanda.Parameters.Add("@GradTima", SqlDbType.NVarChar).Value = objNoviTim.GradTima;
            Komanda.Parameters.Add("@BrojTrofeja", SqlDbType.Int).Value = objNoviTim.BrojTrofeja;

            brojSlogova = Komanda.ExecuteNonQuery();
            Veza.Close();
            Veza.Dispose();

            return (brojSlogova > 0);

        }


    }
}
