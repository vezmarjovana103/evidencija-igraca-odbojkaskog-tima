﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KlasePodataka
{
    public class clsTimLista
    {
      
        private List<clsTim> pListaTimova; 

        
        public List<clsTim> ListaTimova
        {
            get
            {
                return pListaTimova;
            }
            set
            {
                if (this.pListaTimova != value)
                    this.pListaTimova = value;
            }
        }

    
        public clsTimLista()
        {
            pListaTimova = new List<clsTim>(); 

        }

   
        public void DodajElementListe(clsTim objNoviTim)
        {
            pListaTimova.Add(objNoviTim);
        }

        public void ObrisiElementListe(clsTim objTimZaBrisanje)
        {
            pListaTimova.Remove(objTimZaBrisanje);  
        }

        public void ObrisiElementNaPoziciji(int pozicija)
        {
            pListaTimova.RemoveAt(pozicija);
        }

        public void IzmeniElementListe(clsTim objStariTim, clsTim objNoviTim)
        {
            int indexStarogTima = 0;
            indexStarogTima = pListaTimova.IndexOf(objStariTim);
            pListaTimova.RemoveAt(indexStarogTima);
            pListaTimova.Insert(indexStarogTima, objNoviTim);   
        }

           

     




    }
}
