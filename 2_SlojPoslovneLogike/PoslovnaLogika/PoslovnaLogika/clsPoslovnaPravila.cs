﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using KlasePodataka;
using KlaseMapiranja;

namespace PoslovnaLogika
{
    public class clsPoslovnaPravila
    {   
        // atributi
        private string pStringKonekcije;

        // property

        // konstruktor
        public clsPoslovnaPravila(string NoviStringKonekcije)
        {
            pStringKonekcije = NoviStringKonekcije;
        }

        // public metode
        public bool DaLiImaMestaZaDodavanjeNovogIgracaUSastavTima(int IdTimaIzBazePodataka)
        {
            //poslovno pravilo glasi da li ima mesta za dodavanje novog igraca u tim

            bool imaMesta = false;
            // 1. IZRACUNAVANJE TRENUTNOG BROJA igraca U BAZI PODATAKA
            // 
            int UkupnoIgraca = 0;
            clsEvidencijaIgracaDB objEvidencijaIgracaDB = new clsEvidencijaIgracaDB(pStringKonekcije);
            UkupnoIgraca = objEvidencijaIgracaDB.DajUkupnoIgracaPoIdTima(IdTimaIzBazePodataka);

            // 2. MAPIRANJE SLOJEVA - uskladjivanje ID tima iz raznih delova programa
            // Web servis ima 1 id tima, baza podataka ima 1 - docent
            string IdTimWS = "";
            clsMaper objMaper = new clsMaper(pStringKonekcije);
            IdTimWS = objMaper.DajIdTimaZaWebServis(IdTimaIzBazePodataka);

            // 3. IZDVAJANJE MAX BROJA igraca ZA ODG tim
            MaxBrojIgraca.WebService1 objOgranicenja = new MaxBrojIgraca.WebService1();
            int MaxBrojIgraca = objOgranicenja.DajMaxBrojIgracaZaTim(IdTimaIzBazePodataka);
            // ################################################################
            // 4. UPOREDJIVANJE TRENUTNOG BROJA I MAX BROJA igraca
            if (UkupnoIgraca < MaxBrojIgraca)
            {
                imaMesta = true;
            }
            else
            {
                imaMesta = false;
            }
            //  skracena verzija return UkupnoIgraca > MaxBrojIgraca;

            return imaMesta;
        }
            
    }
}
