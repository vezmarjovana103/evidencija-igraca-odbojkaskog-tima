﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//ukljucivanje klase podataka
using KlasePodataka;

namespace KlaseMapiranja
{
    public class clsMaper
    {
       
        private string pStringKonekcije;

        
        public clsMaper(string NoviStringKonekcije)
        {
            pStringKonekcije = NoviStringKonekcije;
        }


        public string DajIdTimaZaWebServis(int IdTimaIzBazePodataka)
        {
            string IdTimaWS = "";
            clsTimDB objTimDB = new clsTimDB(pStringKonekcije);
            string nazivTimaIzBazePodataka = objTimDB.DajNazivTimaPremaIdTima(IdTimaIzBazePodataka);


            IdTimaWS = nazivTimaIzBazePodataka[0].ToString();

            return IdTimaWS;           
        }

    }
}
