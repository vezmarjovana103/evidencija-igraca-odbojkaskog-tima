﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;

namespace MaxBrojIgraca
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {

        [WebMethod]
        public DataSet DajSvaOgranjcenja()
        {
            DataSet dsOgranicenja = new DataSet();
            dsOgranicenja.ReadXml(Server.MapPath("~/") + "XML/OgranicenjaTima.XML");

            return dsOgranicenja;
        }


        [WebMethod]
        public int DajMaxBrojIgracaZaTim(int pomIdTima)
        {
            int MaxBrojIgraca = 0;
            DataSet dsOgranicenja = new DataSet();
            dsOgranicenja.ReadXml(Server.MapPath("~/") + "XML/OgranicenjaTima.XML");

            DataRow[] result = dsOgranicenja.Tables[0].Select("IdTima='" + pomIdTima + "'");
            MaxBrojIgraca = int.Parse(result[0].ItemArray[1].ToString());

            return MaxBrojIgraca;
        }
    }
}
