﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//
using System.Data;
using System.Configuration;
using PrezentacionaLogika;

namespace KorisnickiInterfejs
{
    public partial class EvidencijaIgracaSpisak : System.Web.UI.Page
    {
        // atributi
        //prezentaciona logika
        private clsFormaIgraciSpisak objFormaIgraciSpisak;

        // nase metode
        private void NapuniGrid(DataSet ds)
        {
            gvEvidencijaIgraca.DataSource = ds.Tables[0];
            gvEvidencijaIgraca.DataBind();
            // povezivanje grida sa datasetom
        }
         
        // dogadjaji
        protected void Page_Load(object sender, EventArgs e)
        {
            objFormaIgraciSpisak = new clsFormaIgraciSpisak(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ConnectionString);  
        }

        protected void btnFiltriraj_Click(object sender, EventArgs e)
        {
            NapuniGrid(objFormaIgraciSpisak.DajPodatkeZaGrid(txbFilter.Text));   
        }

        protected void btnSvi_Click(object sender, EventArgs e)
        {
            NapuniGrid(objFormaIgraciSpisak.DajPodatkeZaGrid(""));   
        }
    }
}