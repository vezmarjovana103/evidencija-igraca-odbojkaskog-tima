﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//
using System.Data;
using System.Configuration;
using PrezentacionaLogika;

namespace KorisnickiInterfejs
{
    public partial class EvidencijaIgracaUnos : System.Web.UI.Page
    {
        // atributi
        //prezentaciona logika
        clsFormaIgraciUnos objFormaIgraciUnos;
     
        private void NapuniCombo()
        {
            // IZDVAJANJE PODATAKA IZ XML POSREDSTVOM WEB SERVISA
            DataSet dsTim = new DataSet();
            dsTim = objFormaIgraciUnos.DajPodatkeZaCombo();

            int ukupno = dsTim.Tables[0].Rows.Count;

            // PUNJENJE COMBO PODACIMA IZ DATASETA
            dllTim.Items.Add("Izaberite...");
         
            for (int i = 0; i < ukupno; i++)
            {
                dllTim.Items.Add(dsTim.Tables[0].Rows[i].ItemArray[1].ToString());
            }

        }

        private void IsprazniKontrole()
        {
            txbJMBG.Text = "";
            txbPrezime.Text = "";
            txbIme.Text = "";
            txbDatumRodjenja.Text = "";
            txbBrojDresa.Text = "";
            txbDrzavnoPoreklo.Text = "";
            dllTim.Text ="Izaberite...";
            lblStatus.Text = ""; 
        }


        // dogadjaji
        protected void Page_Load(object sender, EventArgs e)
        {
            objFormaIgraciUnos = new clsFormaIgraciUnos(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ConnectionString);
            if (!IsPostBack)
            {
                NapuniCombo();
            }
        }

        protected void btnSnimi_Click(object sender, EventArgs e)
        {
            // ***********preuzimanje vrednosti sa korisnickog interfejsa
            // 2. nacin - preuzimaju atributi klase prezentacione logike
            objFormaIgraciUnos.JMBG = int.Parse(txbJMBG.Text);
            objFormaIgraciUnos.Prezime = txbPrezime.Text;
            objFormaIgraciUnos.Ime = txbIme.Text;
            objFormaIgraciUnos.DatumRodjenja = txbIme.Text;
            objFormaIgraciUnos.BrojDresa = int.Parse(txbBrojDresa.Text);
            objFormaIgraciUnos.DrzavnoPoreklo = txbDrzavnoPoreklo.Text;
            objFormaIgraciUnos.NazivTima = dllTim.Text;  
            
            // provera ispravnosti vrednosti
            // provera popunjenosti
            bool SvePopunjeno = objFormaIgraciUnos.DaLiJeSvePopunjeno();

            //provera ispravnosti - karakteri, vrednost iz domena, jedinstvenost zapisa
            bool JedinstvenZapis = objFormaIgraciUnos.DaLiJeJedinstvenZapis();


            //provera ispravnosti - provera uskladjenosti podataka sa poslovnim pravilima
            bool UskladjenoSaPoslovnimPravilima = objFormaIgraciUnos.DaLiSuPodaciUskladjeniSaPoslovnimPravilima();

            //snimanje u bazu podataka
            string porukaStatusaSnimanja = "";
            if (SvePopunjeno)
            {
                if (JedinstvenZapis)
                {
                    if (UskladjenoSaPoslovnimPravilima)
                    {
                        // snimanje podataka
                        objFormaIgraciUnos.SnimiPodatke();
                        // priprema teksta poruke o uspehu snimanja
                        porukaStatusaSnimanja = "USPESNO SNIMLJENI PODACI!";
                    }
                    else
                    {
                        porukaStatusaSnimanja = "PODACI NISU U SKLADU SA POSLOVNIM PRAVILIMA!";
                    }
                }
                else
                {
                    porukaStatusaSnimanja = "VEC POSTOJI NASTAVNIK SA ISTIM JMBG!";
                }
            }
            else
            { 
                // priprema teksta poruke o gresci
                porukaStatusaSnimanja = "NISU SVI PODACI POPUNJENI!";
                txbJMBG.Focus();  
            }


            //obavestavanje korisnika o statusu snimanja
            lblStatus.Text = porukaStatusaSnimanja; 


        }

        protected void btnPonisti_Click(object sender, EventArgs e)
        {
            IsprazniKontrole();
        }

        protected void ddlZvanje_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void txbIme_TextChanged(object sender, EventArgs e)
        {

        }
    }
}