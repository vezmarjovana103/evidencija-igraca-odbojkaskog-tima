﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//
using System.Data;
using PrezentacionaLogika;
using System.Configuration; 

namespace KorisnickiInterfejs
{
    public partial class TimDetaljiEdit : System.Web.UI.Page
    {
        // atributi
        private int pId = 0;
        clsFormaTimDetaljiEdit objFormaTimDetaljiEdit; 

        // nase metode
        private void IsprazniKontrole()
        {
            txbId.Text = "";
            txbNazivTima.Text = "";
            txbLigaTima.Text = "";
            txbGradTima.Text = "";
            txbBrojTrofeja.Text = "";
            

        }

        private void AktivirajKontrole()
        {
            txbId.Enabled = true;
            txbNazivTima.Enabled = true;
            txbLigaTima.Enabled = true;
            txbGradTima.Enabled = true;
            txbBrojTrofeja.Enabled = true;
        }

        private void DeaktivirajKontrole()
        {
            txbId.Enabled = false;
            txbNazivTima.Enabled = false;
            txbLigaTima.Enabled = false;
            txbGradTima.Enabled = false;
            txbBrojTrofeja.Enabled = false;
        }

        private void PrikaziPodatke(clsFormaTimDetaljiEdit objFormaTimDetaljiEdit)
        {
            // podacima stranice upravlja klasa prezentacione logike, zato se uzimaju iz nje za prikaz
            txbId.Text = Convert.ToString(objFormaTimDetaljiEdit.IdTimaPreuzetogTima);
            txbNazivTima.Text = objFormaTimDetaljiEdit.NazivPreuzetogTima;
            txbLigaTima.Text = objFormaTimDetaljiEdit.LigaPreuzetogTima;
            txbGradTima.Text = objFormaTimDetaljiEdit.GradPreuzetogTima ;
            txbBrojTrofeja.Text = Convert.ToString(objFormaTimDetaljiEdit.BrojTrofejaPreuzetogTima);
           
          

        }

        // dogadjaji
        protected void Page_Load(object sender, EventArgs e)
        {
            objFormaTimDetaljiEdit = new clsFormaTimDetaljiEdit(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ConnectionString);
            pId = int.Parse(Request.QueryString["Id"].ToString());
            objFormaTimDetaljiEdit.IdTimaPreuzetogTima = pId;
            // OVDE SE NE DOBIJA NAZIV SPOLJA, VEC SE IZRACUNAVA NAZIV na set svojstvu property za sifru UNUTAR KLASE  
            if (!IsPostBack)
            {
                PrikaziPodatke(objFormaTimDetaljiEdit);
            }  
        }

        protected void btnObrisi_Click(object sender, EventArgs e)
        {
            objFormaTimDetaljiEdit.IdTimaPreuzetogTima = int.Parse(txbId.Text);
            bool uspehBrisanja = objFormaTimDetaljiEdit.ObrisiTim();
            if (uspehBrisanja)
            {
                lblStatus.Text = "Uspesno obrisan zapis!";
                IsprazniKontrole();
            }
            else
            {
                lblStatus.Text = "NEUSPEH BRISANJA zapisa!";
            }
        }

        protected void btnIzmeni_Click(object sender, EventArgs e)
        {
            // PREUZIMANJE POCETNIH, STARIH VREDNOSTI - OVDE NEMA POTREBE JER SE URADI NA PAGE LOAD
            //objFormaTimDetaljiEdit.IdTimaPreuzetogTima = Convert.ToInt32(txbId.Text);
            // - ovo se izracuna iz sifre, pa se ne moze ni dodeliti: objFormaZvanjeDetaljiEdit.NazivPreuzetogZvanja = txbNaziv.Text; 
            AktivirajKontrole();
            txbId.Focus();
 
        }

        protected void btnSnimiIzmenu_Click(object sender, EventArgs e)
        {
            objFormaTimDetaljiEdit.IdIzmenjenogTima = int.Parse(txbId.Text);
            objFormaTimDetaljiEdit.NazivIzmenjenogTima = txbNazivTima.Text;
            objFormaTimDetaljiEdit.LigaTimaIzmenjenogTima = txbLigaTima.Text;
            objFormaTimDetaljiEdit.GradTimaIzmenjenogTima = txbGradTima.Text;
            objFormaTimDetaljiEdit.BrojTrofejaIzmenjenogTima = int.Parse(txbBrojTrofeja.Text);

            bool uspehIzmene = objFormaTimDetaljiEdit.IzmeniTim();
            if (uspehIzmene)
            {
                lblStatus.Text = "Uspesno izmenjen zapis!";
                IsprazniKontrole();
            }
            else
            {
                lblStatus.Text = "NEUSPEH BRISANJA zapisa!";
            }
            DeaktivirajKontrole();
        }

       
    }
}