﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//
using System.Data;
using PrezentacionaLogika;
using System.Configuration; 

namespace KorisnickiInterfejs
{
    public partial class TimStampa : System.Web.UI.Page
    {
        // atributi

        // nase metode
        private void NapuniGrid(DataSet ds)
        {
            // povezivanje grida sa datasetom
            gvSpisakTimova.DataSource = ds.Tables[0];
            gvSpisakTimova.DataBind();
         
        }
        
                
        protected void Page_Load(object sender, EventArgs e)
        {
            clsFormaTimStampa objFormaTimStampa = new clsFormaTimStampa(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ConnectionString);
            string filter = Request.QueryString["filter"].ToString();

            if (filter.Equals(""))
            {
                lblNaslov.Text = "SPISAK SVIH TIMOVA"; 
            }
            else
            {
                lblNaslov.Text = "FILTRIRANI SPISAK TIMOVA, naziv=" + filter; 
            }

            NapuniGrid(objFormaTimStampa.DajPodatkeZaGrid(filter)); 
        }
    }
}