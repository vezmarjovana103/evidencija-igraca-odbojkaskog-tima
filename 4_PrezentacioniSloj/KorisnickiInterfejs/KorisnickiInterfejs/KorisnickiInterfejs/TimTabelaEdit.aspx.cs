﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//
using System.Data;
using PrezentacionaLogika;
using System.Configuration; 

namespace KorisnickiInterfejs
{
    public partial class TimTabelaEdit : System.Web.UI.Page
    {
        
        // nase metode
        private void NapuniGrid(DataSet ds)
        {
            // povezivanje grida sa datasetom
            gvSpisakTimovaEdit.DataSource = ds.Tables[0];

            gvSpisakTimovaEdit.DataBind();
        }
        
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                clsFormaTimTabelaEdit objFormaTimTabelaEdit = new clsFormaTimTabelaEdit(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ConnectionString);
                NapuniGrid(objFormaTimTabelaEdit.DajPodatkeZaGrid(""));
            }
        }



        protected void gvSpisakTimovaEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("TimDetaljiEdit.aspx?Sifra=" + gvSpisakTimovaEdit.Rows[gvSpisakTimovaEdit.SelectedIndex].Cells[1].Text);
        }

        
       
    }
}