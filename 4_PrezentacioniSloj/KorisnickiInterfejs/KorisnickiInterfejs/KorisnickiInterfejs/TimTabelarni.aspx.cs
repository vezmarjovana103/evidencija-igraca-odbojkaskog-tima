﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//
using DBUtils;
using System.Configuration;
using System.Data;

namespace KorisnickiInterfejs
{
    public partial class TimTabelarni : System.Web.UI.Page
    {
              
        
        private void NapuniGrid(DataSet ds)
        {
            // povezivanje grida sa datasetom
            gvSpisakTimova.DataSource = ds.Tables[0];
            gvSpisakTimova.DataBind();
        }
        
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnFiltriraj_Click(object sender, EventArgs e)
        {
            KlasePodataka.clsTimDB objTimDB = new KlasePodataka.clsTimDB(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ToString());
            NapuniGrid(objTimDB.DajTimPoNazivu(txbFilter.Text)); 
        }

        protected void btnSvi_Click(object sender, EventArgs e)
        {
            KlasePodataka.clsTimDB clsTimDB = new KlasePodataka.clsTimDB(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ToString());
            NapuniGrid(clsTimDB.DajSveTimove()); 
        }

        
    }
}