﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//
using DBUtils;
using System.Configuration; 

namespace KorisnickiInterfejs
{
    public partial class TimUnos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSnimi_Click(object sender, EventArgs e)
        {
            KlasePodataka.clsTimDB objTimDB = new KlasePodataka.clsTimDB(ConfigurationManager.ConnectionStrings["NasaKonekcija"].ToString());
            KlasePodataka.clsTim objTim = new KlasePodataka.clsTim();
            objTim.IdTima = int.Parse(txbId.Text);
            objTim.NazivTima = txbNazivTima.Text;
            objTim.LigaTima = txbLigaTima.Text;
            objTim.BrojTrofeja = int.Parse(txbBrojTrofeja.Text);
            objTimDB.SnimiNoviTim(objTim);
            lblStatus.Text = "Snimljeno";
        }

        protected void btnOdustani_Click(object sender, EventArgs e)
        {

        }

        protected void txbSifra_TextChanged(object sender, EventArgs e)
        {

        }
    }
}