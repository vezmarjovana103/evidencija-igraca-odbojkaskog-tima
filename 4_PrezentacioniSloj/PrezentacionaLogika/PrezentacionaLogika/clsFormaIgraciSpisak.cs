﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using System.Data;
using KlasePodataka;

namespace PrezentacionaLogika
{
    public class clsFormaIgraciSpisak
    {
        // atributi
        private string pStringKonekcije;

        // property

        // konstruktor
        public clsFormaIgraciSpisak(string NoviStringKonekcije)
        {
            pStringKonekcije = NoviStringKonekcije;
        }

        // private metode

        // public metode
        public DataSet DajPodatkeZaGrid(string filter)
        {
            DataSet dsPodaci = new DataSet();
            clsEvidencijaIgracaDB objEvidencijaIgracaDB = new clsEvidencijaIgracaDB(pStringKonekcije);
            if (filter.Equals(""))
            {
                dsPodaci = objEvidencijaIgracaDB.DajSveEvidencijeIgraca(); 
            }
            else
            {
                dsPodaci = objEvidencijaIgracaDB.DajEvidencijeIgracaPoPrezimenu(filter);
            }
            return dsPodaci;
        }

    }
}
