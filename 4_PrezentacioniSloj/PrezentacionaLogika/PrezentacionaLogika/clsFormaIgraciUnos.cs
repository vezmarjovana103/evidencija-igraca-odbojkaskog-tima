﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using System.Data;
using KlasePodataka;
using PoslovnaLogika;

namespace PrezentacionaLogika
{
    public class clsFormaIgraciUnos
    {
        // atributi
        private string pStringKonekcije;
        private int pJMBG;
        private string pPrezime;
        private string pIme;
        private string pDatumRodjenja;
        private int pBrojDresa;
        private string pDrzavnoPoreklo;
        private string pNazivTima;

        // property
        public int JMBG
        {
            get { return pJMBG; }
            set { pJMBG = value; }
        }
        
        public string Prezime
        {
            get { return pPrezime; }
            set { pPrezime = value; }
        }
        
        public string Ime
        {
            get { return pIme; }
            set { pIme = value; }
        }
        
        public string DatumRodjenja
        {
            get { return pDatumRodjenja; }
            set { pDatumRodjenja = value; }
        }


        public int BrojDresa
        {
            get { return pBrojDresa; }
            set { pBrojDresa = value; }
        }

        public string DrzavnoPoreklo
        {
            get { return pDrzavnoPoreklo; }
            set { pDrzavnoPoreklo = value; }
        }

        public string NazivTima
        {
            get { return pNazivTima; }
            set { pNazivTima = value; }
        }
        
        // konstruktor
        public clsFormaIgraciUnos(string NoviStringKonekcije)
        {
            pStringKonekcije = NoviStringKonekcije;
        }

        // private metode

        // public metode
        public DataSet DajPodatkeZaCombo()
        {
            DataSet dsPodaci = new DataSet();
            clsTimDB objTimDB = new clsTimDB(pStringKonekcije);

            dsPodaci = objTimDB.DajSveTimove();
            
            return dsPodaci;
        }

        public bool DaLiJeSvePopunjeno()
        {
            bool SvePopunjeno = false;

      
            // PRERADA KODA - IF (da li nesto jeste)

            if ((pJMBG > 0) && (pPrezime.Length > 0) && (pIme.Length > 0) && (pDatumRodjenja.Length > 0) && (pBrojDresa > 0) && (pDrzavnoPoreklo.Length > 0) && (pNazivTima.Length > 0) && (!pNazivTima.Equals("Izaberite...")))
            {
                SvePopunjeno = true;
            }
            else
            {
                SvePopunjeno = false;
            }

            return SvePopunjeno;
        }


        public bool DaLiJeJedinstvenZapis()
        {
            bool JedinstvenZapis = false;
            DataSet dsPodaci = new DataSet();
            clsEvidencijaIgracaDB objEvidencijaIgracaDB = new clsEvidencijaIgracaDB(pStringKonekcije);
            dsPodaci = objEvidencijaIgracaDB.DajEvidencijuIgracaPoJMBG(pJMBG);
            
            if (dsPodaci.Tables[0].Rows.Count == 0)
            {
                JedinstvenZapis = true;
            }
            else
            {
                JedinstvenZapis = false;
            }

            return JedinstvenZapis;

        }

        public bool SnimiPodatke()
        {
            bool uspehSnimanja=false;

            clsEvidencijaIgracaDB objEvidencijaIgracaDB = new clsEvidencijaIgracaDB(pStringKonekcije);

            clsEvidencijaIgraca objNovaEvidencijaIgraca = new clsEvidencijaIgraca();
            objNovaEvidencijaIgraca.JMBG = pJMBG;
            objNovaEvidencijaIgraca.Prezime = pPrezime;
            objNovaEvidencijaIgraca.Ime = pIme;
            objNovaEvidencijaIgraca.DatumRodjenja = pDatumRodjenja;
            objNovaEvidencijaIgraca.BrojDresa = pBrojDresa;
            objNovaEvidencijaIgraca.DrzavnoPoreklo = pDrzavnoPoreklo;


            clsTim objTim = new clsTim();

            clsTimDB objTimDB = new clsTimDB(pStringKonekcije);
            objTim.IdTima = Convert.ToInt32(objTimDB.DajIdTimaPoNazivu(pNazivTima));
            objTim.NazivTima = pNazivTima;

            objNovaEvidencijaIgraca.Tim = objTim;

            uspehSnimanja = objEvidencijaIgracaDB.SnimiNovuEvidencijuIgraca(objNovaEvidencijaIgraca);


            return uspehSnimanja;
        }

        public bool DaLiSuPodaciUskladjeniSaPoslovnimPravilima()
        {
            // POSLOVNO PRAVILO:
            // na terenu mora postojati odredjen broj igraca
            // 
            bool UskladjeniPodaci = false;

            clsPoslovnaPravila objPoslovnaPravila = new clsPoslovnaPravila(pStringKonekcije);
            
            //izracunavanje ID zvanja
            clsTimDB objTimDB = new clsTimDB(pStringKonekcije);
            int IdTima = Convert.ToInt32(objTimDB.DajIdTimaPoNazivu(pNazivTima));


            UskladjeniPodaci = objPoslovnaPravila.DaLiImaMestaZaDodavanjeNovogIgracaUSastavTima(IdTima);

            return UskladjeniPodaci;
        }
    }
}
