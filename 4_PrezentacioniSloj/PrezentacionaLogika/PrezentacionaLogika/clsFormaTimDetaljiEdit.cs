﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using System.Data;
using KlasePodataka;

namespace PrezentacionaLogika
{
    public class clsFormaTimDetaljiEdit
    {
   // atributi i property
        private string pStringKonekcije;
        private clsTimDB objTimDB;

        private clsTim objPreuzetTim;
        private clsTim objIzmenjenTim;

        private int pIdPreuzetogTima;
        private string pNazivPreuzetogTima;
        private string pLigaPreuzetogTima;
        private string pGradPreuzetogTima;
        private int pBrojTrofejaPreuzetogTima;

        private int pIdIzmenjenogTima;
        private string pNazivIzmenjenogTima;
        private string pLigaIzmenjenogTima;
        private string pGradIzmenjenogTima;
        private int pBrojTrofejaIzmenjenogTima;
        
// PROPERTY


        public int IdTimaPreuzetogTima
        {
            get { return pIdPreuzetogTima; }
            set { pIdPreuzetogTima = value; 
                pNazivPreuzetogTima = DajNaziv(pIdPreuzetogTima); }
        }


        public string NazivPreuzetogTima
        {
            get { return pNazivPreuzetogTima; }

        }


        public string LigaPreuzetogTima
        {
            get { return pLigaPreuzetogTima; }

        }

        public string GradPreuzetogTima
        {
            get { return pGradPreuzetogTima; }

        }

        public int BrojTrofejaPreuzetogTima
        {
            get { return pBrojTrofejaPreuzetogTima; }

        }

        public int IdIzmenjenogTima
        {
            get { return pIdIzmenjenogTima; }
            set { pIdIzmenjenogTima = value; }
        }
        public string NazivIzmenjenogTima
        {
            get { return pNazivIzmenjenogTima; }
            set { pNazivIzmenjenogTima = value; }
        }

        public string LigaTimaIzmenjenogTima
        {
            get { return pLigaIzmenjenogTima; }
            set { pLigaIzmenjenogTima = value; }
        }

        public string GradTimaIzmenjenogTima
        {
            get { return pGradIzmenjenogTima; }
            set { pGradIzmenjenogTima = value; }
        }

        public int BrojTrofejaIzmenjenogTima
        {
            get { return pBrojTrofejaIzmenjenogTima; }
            set { pBrojTrofejaIzmenjenogTima = value; }
        }



    // konstruktor
        public clsFormaTimDetaljiEdit(string NoviStringKonekcije)
        {
            pStringKonekcije = NoviStringKonekcije;
            objTimDB = new clsTimDB(pStringKonekcije);
        }

        // privatne metode
        private string DajNaziv(int pomIdTima)
        {
            string pomNazivTima ="";
            DataSet dsPodaci = new DataSet();
            pomNazivTima = objTimDB.DajNazivTimaPremaIdTima(pomIdTima);

            return pomNazivTima;
        }

        // javne metode
        public bool ObrisiTim()
        {
            // zvanje koje je trenutno u atributima dato, TJ. preuzeta sifra je bitna
            bool uspehBrisanja = false;
            uspehBrisanja = objTimDB.ObrisiTim(pIdPreuzetogTima);

            return uspehBrisanja;

        }

        public bool IzmeniTim()
        {
            bool uspehIzmene = false;
            objPreuzetTim = new clsTim();
            objIzmenjenTim = new clsTim();

            objPreuzetTim.IdTima = pIdPreuzetogTima;
            objPreuzetTim.NazivTima = pNazivPreuzetogTima;
            objPreuzetTim.LigaTima = pLigaPreuzetogTima;
            objPreuzetTim.GradTima = pGradPreuzetogTima;
            objPreuzetTim.BrojTrofeja = pBrojTrofejaPreuzetogTima;

            objIzmenjenTim.IdTima = pIdIzmenjenogTima;
            objIzmenjenTim.NazivTima = pNazivIzmenjenogTima;
            objIzmenjenTim.LigaTima = pLigaIzmenjenogTima;
            objIzmenjenTim.GradTima = pGradIzmenjenogTima;
            objIzmenjenTim.BrojTrofeja = pBrojTrofejaIzmenjenogTima;

            uspehIzmene = objTimDB.IzmeniTim(objPreuzetTim, objIzmenjenTim);
                //IzmeniZvanje(objPreuzetoZvanje, objIzmenjenoZvanje);  

            return uspehIzmene;
        }
    }
}
