﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using System.Data;
using KlasePodataka;

namespace PrezentacionaLogika
{
    public class clsFormaTimStampa
    {
        // atributi
        private string pStringKonekcije;

        // property

        // konstruktor
        public clsFormaTimStampa(string NoviStringKonekcije)
        {
            pStringKonekcije = NoviStringKonekcije;
        }

        // private metode

        // public metode
        public DataSet DajPodatkeZaGrid(string filter)
        {
            DataSet dsPodaci = new DataSet();
            clsTimDB objTimDB = new clsTimDB(pStringKonekcije);            
            if (filter.Equals(""))
            {
                dsPodaci = objTimDB.DajSveTimove(); 
            }
            else
            {
                dsPodaci = objTimDB.DajTimPoNazivu(filter); 
            }
            return dsPodaci;
        }
    }
}
